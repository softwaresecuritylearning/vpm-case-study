# SVA 2017, Final project: Case Study of Firefox Browser
* Group members: Nan Ding, Sifan Li, Lingyun Zhao
* Instructor: Reuben Johnston

## Prerequisites
* Students will need MATLAB and the following toolboxes
    * Bioinformatics Toolbox
	* Statistics and Machine Learning Toolbox
	* System Identification Toolbox
* Optional
    * Python (for replicating feature extraction from Firefox's Mercurial repository)
	
## Details
* Run VPMs/main.m in MATLAB (Note, results differ depending on the dataset used)
    * To build models on the partial dataset, ensure the 8th line `load('partial_data.mat');` in `main.m` is uncommented and the 11th line `%load('full_data.mat')` is commented out.
    * To build models on the full dataset, ensure the 8th line `%load('partial_data.mat');` in `main.m` is commented out and the 11th line `load('full_data.mat')` is uncommented.
* To replicate feature extraction from Mozilla's Mercurial source code repository
    * Run ExtractFeatures.py using Python

## References
* [Christopher Theisen, Rahul Krishna, Lauri Williams, "Strengthening the Evidence that Attack Surfaces Can Be Approximated with Stack Traces," TR-2015-10, NC State University, Computer Science, November 3, 2015](ftp://ftp.ncsu.edu/pub/unity/lockers/ftp/csc_anon/tech/2015/TR-2015-10.pdf)
* [Christopher Theisen, Kim Herzig, Patrick Morrison, Brendan Murphy, Laurie Williams, "Approximating Attack Surfaces with Stack Traces," Proceedings of the 2015 IEEE International Conference on Software Engineering, Florence, Italy](https://dx.doi.org/10.1109/ICSE.2015.148)