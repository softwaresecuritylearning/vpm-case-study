function [precision, recall, tn_rate, accuracy] = evaluate(pred, labels)

tp = sum((pred == 1) & (labels == 1));
tn = sum((pred == 0) & (labels == 0));
fn = sum((pred == 0) & (labels == 1));
fp = sum((pred == 1) & (labels == 0));

precision = tp / (tp + fp);
recall = tp / (tp + fn);
tn_rate = tn / (tn + fp);
accuracy = (tp + tn) / (tp + tn + fn + fp);