clear; close all; clc

fprintf('********** Loading data **********\n\n');

T = 100;

% train models using partial dataset
% load('partial_data.mat');

% train models using full dataset
load('full_data.mat');

fprintf('********** Decision Tree Model **********\n');

fprintf('Train CT model...\n');
rng(1); % For reproducibility
ctreemdl = fitctree(data(:, 1:3), data(:, 4), 'OptimizeHyperparameters', 'auto', ...
    'PredictorNames',{'CrashAmount','Churn','Unique'}, 'ResponseName','SecurityFlaw');
ctreemdl = prune(ctreemdl);%, 'Level', 15);
view(ctreemdl,'mode','graph');
% 
% fprintf('Predict...\n');
% pred = predict(ctreemdl, ts_data(:, 1:3));
% 
% fprintf('Evaluate...\n');
% [precision, recall, tn_rate, accuracy] = evaluate(pred, ts_data(:, 4));
% fprintf('precision = %f\nrecall = %f\ntrue negative rate = %f\naccuracy = %f\n\n', ...
%     precision, recall, tn_rate, accuracy);

fprintf('********** Random Forest Model w/ or w/o SMOTEing **********\n');

fprintf('10-fold Cross-Validation w/o SMOTEing...\n');

precision = zeros(1, 10);
recall = zeros(1, 10);
tn_rate = zeros(1, 10);
accuracy = zeros(1, 10);

indices = crossvalind('Kfold', data(:, 4), 10);
for k = 1:10
    test = (indices == k);
    train = ~test;
    train_data = data(train, :);
    test_data = data(test, :);
    rfmdl = RandomForest(train_data);
    pred = predict(rfmdl, test_data(:, 1:3));
    pred = cellfun(@str2num, pred);
    [precision(k), recall(k), tn_rate(k), accuracy(k)] = evaluate(pred, test_data(:, 4));
end
fprintf('precision: median = %f, IQR = %f\nrecall: median = %f, IQR = %f\ntrue negative rate: median = %f, IQR = %f\naccuracy: median = %f, IQR = %f\n\n', ...
    mean(precision), iqr(precision), mean(recall), iqr(recall), mean(tn_rate), iqr(tn_rate), mean(accuracy), iqr(accuracy));

fprintf('10-fold Cross-Validation w/ SMOTEing...\n');

precision = zeros(1, 10);
recall = zeros(1, 10);
tn_rate = zeros(1, 10);
accuracy = zeros(1, 10);

indices = crossvalind('Kfold', data(:, 4), 10);
for k = 1:10
    test = (indices == k);
    train = ~test;
    train_data = data(train, :);
    test_data = data(test, :);
    rfmdl = RandomForest(SMOTE(train_data, T, 5));
    pred = predict(rfmdl, test_data(:, 1:3));
    pred = cellfun(@str2num, pred);
    [precision(k), recall(k), tn_rate(k), accuracy(k)] = evaluate(pred, test_data(:, 4));
end
fprintf('precision: median = %f, IQR = %f\nrecall: median = %f, IQR = %f\ntrue negative rate: median = %f, IQR = %f\naccuracy: median = %f, IQR = %f\n\n', ...
    mean(precision), iqr(precision), mean(recall), iqr(recall), mean(tn_rate), iqr(tn_rate), mean(accuracy), iqr(accuracy));

fprintf('********** Naive Bayes Model w/ or w/o SMOTEing **********\n');

fprintf('10-fold Cross-Validation w/o SMOTEing...\n');

precision = zeros(1, 10);
recall = zeros(1, 10);
tn_rate = zeros(1, 10);
accuracy = zeros(1, 10);

indices = crossvalind('Kfold', data(:, 4), 10);
for k = 1:10
    test = (indices == k);
    train = ~test;
    train_data = data(train, :);
    test_data = data(test, :);
    nbmdl = fitcnb(train_data(:, 1:3), train_data(:, 4));
    pred = predict(nbmdl, test_data(:, 1:3));
    [precision(k), recall(k), tn_rate(k), accuracy(k)] = evaluate(pred, test_data(:, 4));
end
fprintf('precision: median = %f, IQR = %f\nrecall: median = %f, IQR = %f\ntrue negative rate: median = %f, IQR = %f\naccuracy: median = %f, IQR = %f\n\n', ...
    mean(precision), iqr(precision), mean(recall), iqr(recall), mean(tn_rate), iqr(tn_rate), mean(accuracy), iqr(accuracy));

fprintf('10-fold Cross-Validation w/ SMOTEing...\n');

precision = zeros(1, 10);
recall = zeros(1, 10);
tn_rate = zeros(1, 10);
accuracy = zeros(1, 10);

indices = crossvalind('Kfold', data(:, 4), 10);
for k = 1:10
    test = (indices == k);
    train = ~test;
    train_data = data(train, :);
    test_data = data(test, :);
    train_data = SMOTE(train_data, T, 5);
    nbmdl = fitcnb(train_data(:, 1:3), train_data(:, 4));
    pred = predict(nbmdl, test_data(:, 1:3));
    [precision(k), recall(k), tn_rate(k), accuracy(k)] = evaluate(pred, test_data(:, 4));
end
fprintf('precision: median = %f, IQR = %f\nrecall: median = %f, IQR = %f\ntrue negative rate: median = %f, IQR = %f\naccuracy: median = %f, IQR = %f\n\n', ...
    mean(precision), iqr(precision), mean(recall), iqr(recall), mean(tn_rate), iqr(tn_rate), mean(accuracy), iqr(accuracy));