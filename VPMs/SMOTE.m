function Synthetic = SMOTE(training, N, k)

Minority = training(training(:, end) == 1, :);
Minority = Minority(:,1:end-1);
Majority = training(training(:, end) == 0, :);
T = size(Minority, 1);
N = floor(N / 100);
Synthetic = zeros(floor(T * N), size(Minority, 2));

% under-sample the majority class
Majority = Majority(randperm(size(Majority, 1), floor(T * (N + 1))), :);
% Majority = [Majority zeros(size(Majority, 1))];

% compute distances between each minority sample
dists = sqrt(-2 * (Minority * Minority') + sum(Minority .^ 2, 2) + sum(Minority' .^ 2));
% get the indices of the k-nearest neighbors of each minority sample
[~, I] = sort(dists, 2);
I = I(:, 2:k+1);

for i = 1:N
    neighbors = I(sub2ind(size(I), (1:T)', randi(k, T, 1)));
    Synthetic((i-1)*T+1:i*T,:) = floor(Minority + rand(size(Minority)) .* (Minority(neighbors) - Minority));
end

Synthetic = [Synthetic ones(size(Synthetic, 1), 1)];
% Synthetic = [training; Synthetic];
Synthetic = [Majority; Synthetic];
Synthetic = [Minority ones(T, 1); Synthetic];
Synthetic = Synthetic(randperm(size(Synthetic, 1)), :);